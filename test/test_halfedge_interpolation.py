import numpy as np
import copy

from fealpy.mesh import HalfEdgeMesh2d, MeshFactory
from fealpy.functionspace import LagrangeFiniteElementSpace
from fealpy.decorator import cartesian
import matplotlib.pyplot as plt
@cartesian
def u(p):
    x = p[..., 0]
    y = p[..., 1]
    return np.sin(x*y)-y**6

#生成网格
mesh = MeshFactory.boxmesh2d([-1, 1, -1, 1], 3, 3, meshtype="tri")
mesh = HalfEdgeMesh2d.from_mesh(mesh)
mesh.ds.NV = 3

#加密初始网格获得细网格
NC = mesh.number_of_all_cells()
isMarkedCell = np.ones(NC, dtype=np.bool_)
mesh.refine_triangle_rg(isMarkedCell)

mesh0 = copy.deepcopy(mesh) #备份一个细网格
mesh1 = copy.deepcopy(mesh) #备份一个细网格

#细网格上的空间
space0 = LagrangeFiniteElementSpace(mesh0, p=1)
u0 = space0.interpolation(u) #原始的细网格的插值
u00 = space0.function() #将粗网格上的解插值到细网格上

#fig = plt.figure()
#axes = fig.add_subplot(1, 1, 1, projection='3d')
#u0.add_plot(axes, cmap='rainbow')

#粗化网格
NC = mesh.number_of_all_cells()
isMarkedCell = np.ones(NC, dtype=np.bool_)
isMarkedCell[1:4] = False
mesh.coarsen_triangle_rg(isMarkedCell)

#粗网格上的空间
space1 = LagrangeFiniteElementSpace(mesh, p=1)
u1 = space1.interpolation(u)

fig = plt.figure()
axes = fig.add_subplot(1, 1, 1, projection='3d')
u1.add_plot(axes, cmap='rainbow')

halfedge = mesh.entity("halfedge")

# 插值
rnidx = mesh.retainnode #粗网格点的原始编号
dnidx = mesh.deletenode #细网格中被删除的点的编号(在细网格中的编号)
dn2e = mesh.deletnode2edge
print(dnidx)
print(dn2e)

u00[rnidx] = u1[:]
u00[dnidx] = 0.5*(u1[halfedge[dn2e, 0]] + u1[halfedge[halfedge[dn2e, 3], 0]])
print(u00[:])


fig = plt.figure()
axes = fig.add_subplot(1, 1, 1, projection='3d')
u00.add_plot(axes)
plt.show()
