
import numpy as np
from fealpy.mesh import TriangleMesh
from fealpy.functionspace import RaviartThomasFiniteElementSpace2d
import sys

import matplotlib.colors as colors
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

node = np.array([[0, 0], [1, 0], [1, 1], [0, 1]], dtype=np.float_)
cell = np.array([[0, 1, 2], [0, 2, 3]], dtype=np.int_)
mesh = TriangleMesh(node, cell)
#mesh.uniform_refine(int(sys.argv[1]))

space = RaviartThomasFiniteElementSpace2d(mesh, p=1)
dof = space.dof

bc = np.array([[0.2, 0.0, 0.8]])

#print(mesh.entity('edge'))
#print(dof.cell2dof)
#print(space.basis_vector())
#print(space.basis(bc))
bc = np.array([[0.2, 0.8, 0.0]])
#print(space.basis(bc))

def ff(p):
    x = p[..., 0, np.newaxis]
    y = p[..., 1, np.newaxis]
    #return np.c_[np.sin(x)*y, y-x-1]
    return np.c_[x**3*y*np.sin(x), y-x-1]

print("L2投影误差")
fl = space.project(ff)
err0 = space.integralalg.L2_error(ff, fl)
for i in range(5):
    mesh.uniform_refine()
    space = RaviartThomasFiniteElementSpace2d(mesh, p=0)
    fl = space.project(ff)
    err1 = err0.copy()
    err0 = space.integralalg.L2_error(ff, fl)
    print(np.log(err1/err0)/np.log(2))










